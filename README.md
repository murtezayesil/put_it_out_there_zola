# put_it_out_there Theme for Zola

Port [put_it_out_there theme for Pelican](https://gitlab.com/murtezayesil/put_it_out_there) to Zola.

You can view [the demo site](https://zola.murtezayesil.me) as an example for default configuration and [my blog](https://murtezayesil.me) as a customized configuration.

## Installation

First download this theme to your `themes` directory:

```bash
$ cd themes
$ git clone https://gitlab.com/murtezayesil/put_it_out_there_zola.git
```
or download the zip file for the latest release from [the releases page](https://gitlab.com/murtezayesil/put_it_out_there_zola/-/releases/) and extract into themes directory.

Then enable it in your `config.toml`:

```toml
theme = "put_it_out_there_zola"
```

## Options

Below options should be placed under `[extra]` section in `config.toml`


### Language

Set the language of the site. Examples:
`language = "en"` for English language.
`language = "zh"` for Chinese language.


### Title

Because there is no title in default config file for some reason.
`title = "TITLE"`


### Menu items

Items to be shown in the top menu
```toml
menu_items = [
	{ url = "https://zola.murtezayesil.me/about/", name = "About"},
	{ url = "https://zola.murtezayesil.me/contact/", name = "Contact"},
	{ url = "https://zola.murtezayesil.me/archive/", name = "Archive"},
]
```


### Categories Submenu (not implemented)

Show a second menu with categories
`show_categories_submenu = false`


### License

Remember to mention license used for the blog posts in this site.
This will help people know how they should treat the quotes taken from this site.
`license = "CC BY-SA 4.0"` for using [Creative Commons -- Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
Find an appropriate license [here](https://creativecommons.org/share-your-work/).

## Planned features
- RSS feed
- sorting articles by date (currently it is mixed)
- Pagination
- Comment via Mastodon toot
- archieve page
- 512KB Club banner
